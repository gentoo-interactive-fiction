# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="6"
PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

DESCRIPTION="Pure-Python implementation of the Treaty of Babel"
HOMEPAGE="http://pyifbabel.invergo.net/"
SRC_URI="http://pyifbabel.invergo.net/download/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="!<games-util/grotesque-0.9.5.1"
RDEPEND="${DEPEND}"

PATCHES=( "${FILESDIR}"/pyifbabel-0.4-get_resources.patch )

python_prepare() {
	python_fix_shebang .
	sed -i -e "s,doc_dir = .*,doc_dir = '/usr/share/doc/${PF}'," setup.py || die "sed failed"
}

python_install_all() {
	distutils-r1_python_install_all
	rm "${D}/usr/share/doc/${PF}/COPYING" || die "rm COPYING failed"
}
