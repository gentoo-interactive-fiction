# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake-utils

DESCRIPTION="A Software MIDI Synthesizer library with OPL3 (YMF262) emulation"
HOMEPAGE="https://github.com/Wohlstand/libADLMIDI"
COMMIT=e62c5354bdd09564a01ec4660ca90baf02e0d7f8
SRC_URI="https://github.com/Wohlstand/libADLMIDI/archive/${COMMIT}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

BDEPEND="
	virtual/pkgconfig
"
DEPEND=""
RDEPEND="${DEPEND}"

DOCS=( AUTHORS README.md )

S="${WORKDIR}/libADLMIDI-${COMMIT}"

src_configure() {
	local mycmakeargs=(
		-DlibADLMIDI_SHARED=on
		-DlibADLMIDI_STATIC=off
		-DWITH_CPP_EXTRAS=on
	)
	cmake-utils_src_configure
}
