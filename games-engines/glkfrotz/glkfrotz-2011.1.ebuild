# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit toolchain-funcs games

DESCRIPTION="Glk-based interpreter for Z-code based text games"
HOMEPAGE="http://frotz.sourceforge.net/ http://ccxvii.net/gargoyle/ http://code.google.com/p/garglk/"
SRC_URI="http://garglk.googlecode.com/files/gargoyle-${PV}-sources.zip"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=dev-games/glkloader-0.3.2-r3
	!<dev-games/cheapglk-0.9.0-r1
	!=dev-games/garglk-2006*
	!=dev-games/garglk-2008*
	!<dev-games/glkterm-0.8.0-r1
	!<dev-games/glktermw-0.8.0-r2
	!<dev-games/xglk-0.4.11-r1"
DEPEND="${RDEPEND}
	app-arch/unzip
	>=dev-games/glk-headers-0.7.0-r1"

S=${WORKDIR}/terps/frotz

src_compile() {
	$(tc-getCC) ${CPPFLAGS} -I/usr/include/glk ${CFLAGS} *.c -o glkfrotz ${LDFLAGS} -lglkloader || die "cc failed"
}

src_install() {
	dogamesbin glkfrotz || die "dogamesbin failed"
	dodoc README TODO AUTHORS || die "dodoc failed"
	prepgamesdirs
}
