# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit eutils

DESCRIPTION="Yet another Z-machine interpreter"
HOMEPAGE="http://www.spellbreaker.org/~chrender/fizmo/"
SRC_URI="http://www.spellbreaker.org/~chrender/fizmo/source/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="+aiff +jpeg +png +X"
REQUIRED_USE="jpeg? ( X ) png? ( X )"

RDEPEND="jpeg? ( virtual/jpeg:0 )
	png? ( media-libs/libpng )
	X? ( x11-libs/libX11 )
	sys-libs/ncurses[unicode]
	media-libs/libsdl2
	aiff? ( media-libs/libsndfile )
	dev-libs/libxml2"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

DOCS="CHANGELOG.txt LICENSE.txt README.txt user-config-example.txt"

src_configure() {
	econf \
		$(use_enable aiff) \
		$(use_enable jpeg) \
		$(use_enable png) \
		$(use_enable X x11)
}

src_install() {
	emake DESTDIR="${D}" install
	dodoc ${DOCS}
}
