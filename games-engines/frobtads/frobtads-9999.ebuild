# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
if [ "$PV" == "9999" ]; then
	SRC_URI=""
	EGIT_REPO_URI="https://github.com/realnc/frobtads.git"
	KEYWORDS=""
	inherit autotools git-r3
fi
inherit eutils

DESCRIPTION="Curses-based interpreter and development tools for TADS 2 and TADS 3 text adventures"
HOMEPAGE="http://www.tads.org/frobtads.htm"

if [ "$PV" != "9999" ]; then
	SRC_URI="https://github.com/realnc/${PN}/releases/download/${PV}/${P}.tar.bz2"
	KEYWORDS="~amd64 ~x86"
fi

LICENSE="TADS2 TADS3"
SLOT="0"
RESTRICT="!tads3compiler? ( test )"
IUSE="debug frobd +tads2compiler +tads3compiler"

DEPEND="sys-libs/ncurses:0
	net-misc/curl"
RDEPEND="${DEPEND}"

DOCS=( doc/{AUTHORS,BUGS,ChangeLog.old,NEWS,README,SRC_GUIDELINES,THANKS} )

src_unpack() {
	if [ "${PV}" == "9999" ]; then
		git-r3_src_unpack
		cd "${S}"
		eautoreconf
	else
		base_src_unpack
	fi
}

src_configure() {
	econf \
		$(use_enable debug t3debug) \
		$(use_enable frobd) \
		$(use_enable tads2compiler t2-compiler) \
		$(use_enable tads3compiler t3-compiler)
}

src_test() {
	#if use debug; then
	#	emake check || die "TADS 3 compiler test suite failed"
	#fi

	emake sample
	./frob -i plain -p samples/sample.t3 <<- END_FROB_TEST
		save
		testsave.sav
		restore
		testsave.sav
	END_FROB_TEST
	[[ $? -eq 0 ]] || die "Failed to run test game"
}
