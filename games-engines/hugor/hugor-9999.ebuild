# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

if [ "$PV" != "9999" ]; then
	SRC_URI="https://github.com/realnc/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
else
	SRC_URI=""
	KEYWORDS=""
fi

inherit desktop qmake-utils xdg

if [ "$PV" == "9999" ]; then
	EGIT_REPO_URI="https://github.com/realnc/hugor.git"
	inherit git-r3
fi

DESCRIPTION="Multimedia interpreter for Hugo text adventures"
HOMEPAGE="http://ifwiki.org/index.php/Hugor"

LICENSE="GPL-3+"
SLOT="0"
IUSE="gstreamer +vlc"

BDEPEND="
	virtual/pkgconfig
"
DEPEND="
	dev-qt/qtwidgets:5
	gstreamer? ( media-libs/gst-plugins-base:1.0 )
	media-libs/libadlmidi
	media-libs/libmodplug
	media-libs/libsdl2[sound]
	media-libs/libsndfile
	>=media-sound/fluidsynth-2:=
	media-sound/mpg123
	vlc? ( || ( media-video/vlc[ffmpeg] media-video/vlc[libav] ) )
	x11-misc/shared-mime-info
"
RDEPEND="
	gstreamer? (
		media-libs/gst-plugins-good:1.0
		media-plugins/gst-plugins-libav
	)
	${DEPEND}
"

src_configure() {
	local conf
	# VLC is slightly better, so we prefer it.
	if use vlc; then
		conf="vlc-video"
	elif use gstreamer; then
		conf="gstreamer-video"
	else
		conf="disable-video"
	fi
	if use gstreamer && use vlc; then
		elog "Both the gstreamer and the vlc USE flags are set. Hugor "
		elog "will be built with vlc, since it's slightly better."
	fi
	eqmake5 PREFIX="${EPREFIX}/usr" \
		DOCDIR="${EPREFIX}/usr/share/doc/${PF}" \
		CONFIG+="${conf} adlmidi modplug" \
		-after CONFIG-=silent
}

src_install() {
	emake INSTALL_ROOT="${D}" install
}
