# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

#EAPI=1

inherit eutils versionator

MY_MAJOR=$(get_version_component_range 2)
MY_LETTER=$(get_version_component_range 3)
MY_MINOR=$(get_version_component_range 4)

MY_LOWERS=abcdefghijklmnopqrstuvwxyz
MY_UPPERS=ABCDEFGHIJKLMNOPQRSTUVWXYZ
MY_INDEX=${MY_LOWERS%${MY_LETTER}*}
MY_UPPER=${MY_UPPERS:${#MY_INDEX}:1}

MY_PV=${MY_MAJOR}${MY_UPPER}${MY_MINOR#p}
DESCRIPTION="Design system for interactive fiction"
HOMEPAGE="http://inform7.com/"
SRC_URI="x86? ( http://inform7.com/download/content/${MY_PV}/I7_${MY_PV}_Linux_i386.tar.gz )
	!x86? ( http://inform7.com/download/content/${MY_PV}/I7_${MY_PV}_Linux_all.tar.gz )"

# "Inform" for the core, "GPL-2" for the i7 user-interface script
LICENSE="Inform GPL-2"
SLOT="7"
#KEYWORDS="-* ~amd64 ~arm ~ppc ~s390 ~x86"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

RESTRICT="strip"

DEPEND=""
# i7 is written in perl, and uses uuidgen from e2fsprogs
RDEPEND="dev-lang/perl
	sys-fs/e2fsprogs
	games-engines/glkfrotz
	games-engines/glulxe"
	# Not released yet, use bundled for now
	#>=dev-lang/inform-6.32:6

S=${WORKDIR}/inform7-${MY_PV}

src_unpack() {
	unpack ${A}
	cd "${S}"

	mkdir usr
	cd usr

	case ${CHOST} in
		# XXX if the arm team wants to keyword this, someone who knows
		# how these things work should probably restrict this to
		# compatible CHOSTS
		arm*-*)     MY_ARCH=armv5tel ;;
		i?86-*)     MY_ARCH=i386     ;;
		# XXX does it work on ppc64? (statically linked, so doesn't
		# need 32bit libs)
		powerpc*-*) MY_ARCH=ppc      ;;
		s390-*)     MY_ARCH=s390     ;;
		s390x-*)    MY_ARCH=s390x    ;;
		x86_64-*)   MY_ARCH=x86_64   ;;
		*) die "unsupported CHOST"
	esac

	unpack ./../inform7-common_${MY_PV}_all.tar.gz
	unpack ./../inform7-compilers_${MY_PV}_${MY_ARCH}.tar.gz
	# We don't currently use anything from here, but a future version
	# might include more interpreters.
	unpack ./../inform7-interpreters_${MY_PV}_${MY_ARCH}.tar.gz

	epatch "${FILESDIR}"/inform7-5J39-paths.patch
	cd share/inform7/Inform7/Extensions # grumble
	epatch "${FILESDIR}"/glulx-statuswin.patch
}

src_install() {
	cp -pPR usr "${D}" || die "cp usr failed"
	cd "${D}"/usr

	dodoc share/doc/inform7/README || die "dodoc README failed"
	rm share/doc/inform7/README
	docinto ChangeLogs
	dodoc share/doc/inform7/ChangeLogs/*.txt || die "dodoc ChangeLogs failed"
	rm share/doc/inform7/ChangeLogs/*.txt
	rmdir share/doc/inform7/ChangeLogs
	rm share/doc/inform7/INSTALL
	rmdir share/doc/inform7 || die "rmdir doc/inform7 failed"

	# These seem to be useless with the CLI version.
	rm share/inform7/{map,scene}_icons/*.png || die "rm *.png failed"
	rmdir share/inform7/{map,scene}_icons || die "rmdir icons failed"
	rm share/inform7/Welcome\ Background.png || die "rm failed"

	mv share/inform7/{doc_images,Documentation} || die "mv failed"
	if use doc; then
		mv share/inform7/Documentation share/doc/${PF}/html || die "mv Documentation failed"
		dosym /usr/share/doc/${PF}/html /usr/share/inform7/Documentation || die "dosym Documentation failed"
	else
		rm -r share/inform7/Documentation || die "rm -r Documentation failed"
	fi

	# Don't use the bundled interpreter binaries.  We do, however,
	# keep the bundled Inform 6 compiler, because it has some
	# modifications to work with Inform 7-generated code (these will
	# be released with Inform 6.32).
	#
	# The binaries that we do keep are unpacked into /usr/share with
	# symlinks in /usr/libexec, which is silly; move them to libexec
	# (there doesn't seem to be any need to recreate the symlinks
	# going the other way: the i7 script accesses them from libexec).
	# List the kept binaries explicitly (instead of using a wildcard)
	# so that if a future version adds more, the rmdir will fail and
	# alert the maintainer that changes (new dependencies etc) might
	# be required.
	rm libexec/* || die "rm libexec failed"
	mv share/inform7/Compilers/{ni,cBlorb,inform-6.31-biplatform} libexec || die "mv Compilers failed"
	rmdir share/inform7/Compilers || die "rmdir Compilers failed"
	rm share/inform7/Interpreters/{dumb-frotz,dumb-glulxe}
	rmdir share/inform7/Interpreters || die "rmdir Interpreters failed"

	mv man share/man || die "mv man failed"
}
