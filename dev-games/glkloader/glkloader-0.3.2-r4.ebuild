# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils autotools

DESCRIPTION="A Glk library which dynamically loads another Glk library"
HOMEPAGE="http://www.eblong.com/zarf/glk/"
SRC_URI="mirror://ifarchive/programming/glk/implementations/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
IUSE=""
KEYWORDS="~amd64 ~x86"

DEPEND=">=dev-games/glk-headers-0.7.4"
RDEPEND="virtual/glk"

src_unpack() {
	unpack ${A}
	cd "${S}"

	epatch "${FILESDIR}"/glkloader-0.3.2-double-free.patch
	epatch "${FILESDIR}"/glkloader-0.3.2-glk-0.7.4.patch
	epatch "${FILESDIR}"/glkloader-0.3.2-r1-garglk-extensions.patch
	epatch "${FILESDIR}"/glkloader-0.3.2-dynamic-blorb-dispa.patch
	epatch "${FILESDIR}"/glkloader-0.3.2-r4-cache-function-pointers.patch
	epatch "${FILESDIR}"/glkloader-0.3.2-no-rdynamic.patch

	cp "${FILESDIR}"/glkloader-0.3.2-configure.ac configure.ac || die "cp configure.ac failed"
	cp "${FILESDIR}"/glkloader-0.3.2-r1-Makefile.am Makefile.am || die "cp Makefile.am failed"
	sed -i -e "s,@PV@,${PV}," configure.ac || die "sed failed"
	eautoreconf

	rm glk.h gi_blorb.h gi_dispa.h || die "rm glk headers failed"
}

src_compile() {
	cd xpconfig || die "cd xpconfig failed"
	econf --with-method=rcfile || die "econf xpconfig failed"
	emake || die "emake xpconfig failed"

	cd .. || die "cd failed"
	econf || die "econf failed"
	emake || die "emake failed"
}

src_install() {
	cd xpconfig || die "cd xpconfig failed"
	emake DESTDIR="${D}" install || die "emake install xpconfig failed"

	cd .. || die "cd failed"
	emake DESTDIR="${D}" install || die "emake install failed"
	dodoc ChangeLog README || die "dodoc failed"
	docinto makefile-samples
	dodoc makefile-samples/* || die "dodoc makefile-samples failed"

	keepdir /etc/glkloader.d
	insinto /etc
	cat >"${T}"/glkloaderrc <<-EOF
		[Default]
		libs = Gargoyle TermW Term Cheap
	EOF
	doins "${T}"/glkloaderrc || die "doins glkloaderrc failed"
}
