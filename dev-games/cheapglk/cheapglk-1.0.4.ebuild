# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils autotools

MY_P=${P//./}
DESCRIPTION="A stdio implementation of the Glk API"
HOMEPAGE="http://www.eblong.com/zarf/glk/"
SRC_URI="mirror://ifarchive/programming/glk/implementations/${MY_P}.tar.gz"

LICENSE="Zarf"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=dev-games/glk-headers-0.7.4"
RDEPEND=""

S=${WORKDIR}/${PN}

src_unpack() {
	unpack ${A}
	cd "${S}"

	epatch "${FILESDIR}"/cheapglk-1.0.4-garglk-extensions.patch

	cp "${FILESDIR}"/cheapglk-0.9.0-configure.ac configure.ac || die "cp configure.ac failed"
	cp "${FILESDIR}"/cheapglk-1.0.4-Makefile.am Makefile.am || die "cp Makefile.am failed"
	sed -i -e "s,@PV@,${PV}," configure.ac || die "sed failed"
	eautoreconf

	rm glk.h gi_blorb.h gi_dispa.h glkstart.h || die "rm glk headers failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	dodoc notes.txt || die "dodoc failed"

	insinto /etc/glkloader.d
	cat >"${T}"/cheapglk.rc <<-EOF
		[Cheap]
		path = /usr/$(get_libdir)/libcheapglk.so
	EOF
	doins "${T}"/cheapglk.rc || die "doins cheapglk.rc failed"
}
