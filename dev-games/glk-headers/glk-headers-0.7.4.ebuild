# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="Headers for the Glk API"
HOMEPAGE="http://www.eblong.com/zarf/glk/"
SRC_URI="" # no versioned download as far as I know, so in ${FILESDIR}

LICENSE="Zarf GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="!=dev-games/glkloader-0.3.2"
RDEPEND="${DEPEND}"

src_unpack() {
	mkdir "${S}"
	cd "${S}" || die "cd failed"
	cp "${FILESDIR}"/${PV}/*.h . || die "cp failed"

	epatch "${FILESDIR}"/glk-headers-0.7.4-garglk-extensions-2011.1.patch
}

src_install() {
	insinto /usr/include/glk
	doins *.h || die "doins failed"
}
