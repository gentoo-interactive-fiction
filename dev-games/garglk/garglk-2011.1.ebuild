# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=1

inherit eutils autotools

DESCRIPTION="A typographically beautiful Glk library"
HOMEPAGE="http://ccxvii.net/gargoyle/ http://code.google.com/p/garglk/"
SRC_URI="http://garglk.googlecode.com/files/gargoyle-${PV}-sources.zip"

# XXX LICENSE?
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="sdl"

RDEPEND="media-libs/freetype:2
	x11-libs/gtk+:2
	virtual/jpeg:0
	media-libs/libpng
	sdl? ( media-libs/sdl-mixer
		media-libs/sdl-sound )"
DEPEND="${RDEPEND}
	app-arch/unzip
	>=dev-games/glk-headers-0.7.4"
RDEPEND="${RDEPEND}
	media-fonts/liberation-fonts
	media-fonts/libertine"

S=${WORKDIR}/garglk

src_unpack() {
	unpack ${A}
	cd "${S}"

	cp "${FILESDIR}"/garglk-20081225-configure.ac configure.ac || die "cp configure.ac failed"
	cp "${FILESDIR}"/garglk-2011.1-Makefile.am Makefile.am || die "cp Makefile.am failed"
	sed -i -e "s,@PV@,${PV}," configure.ac || die "sed failed"
	eautoreconf

	rm glk.h gi_blorb.h gi_dispa.h glkstart.h || die "rm glk headers failed"

	sed -i \
		-e s/Git/GlulxGit/ -e s/git/glulxgit/ \
		garglk.ini || die "sed garglk.ini failed"
	sed -i \
		-e 's/Linux Libertine O/Linux Libertine/' \
		garglk.ini config.c || die "sed Linux Libertine failed"
}

src_compile() {
	econf $(use_with sdl) || die "econf failed"
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	dodoc TODO || die "dodoc failed"
	insinto /etc
	doins garglk.ini || die "doins garglk.ini failed"

	insinto /etc/glkloader.d
	cat >"${T}"/garglk.rc <<-EOF
		[Gargoyle]
		path = /usr/$(get_libdir)/libgarglk.so
	EOF
	doins "${T}"/garglk.rc || die "doins garglk.rc failed"
}
