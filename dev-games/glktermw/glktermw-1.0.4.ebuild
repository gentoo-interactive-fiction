# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit eutils autotools

MY_P=${P//./}
DESCRIPTION="An ncurses implementation of the Glk API with Unicode support"
HOMEPAGE="http://www.eblong.com/zarf/glk/"
SRC_URI="mirror://ifarchive/programming/glk/implementations/${MY_P}.tar.gz"

LICENSE="Zarf"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="sys-libs/ncurses[unicode]"
DEPEND="${RDEPEND}
	>=dev-games/glk-headers-0.7.4"

S=${WORKDIR}/${PN%w}

src_prepare() {
	epatch "${FILESDIR}"/glktermw-1.0.4-garglk-extensions.patch

	cp "${FILESDIR}"/glktermw-0.8.0-configure.ac configure.ac || die "cp configure.ac failed"
	cp "${FILESDIR}"/glktermw-1.0.4-Makefile.am Makefile.am || die "cp Makefile.am failed"
	sed -i -e "s,@PV@,${PV}," configure.ac || die "sed failed"
	eautoreconf

	rm glk.h gi_blorb.h gi_dispa.h glkstart.h || die "rm glk headers failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	dodoc readme.txt readme-widechar.txt || die "dodoc failed"

	insinto /etc/glkloader.d
	cat >"${T}"/glktermw.rc <<-EOF
		[TermW]
		path = /usr/$(get_libdir)/libglktermw.so
	EOF
	doins "${T}"/glktermw.rc || die "doins glktermw.rc failed"
}
