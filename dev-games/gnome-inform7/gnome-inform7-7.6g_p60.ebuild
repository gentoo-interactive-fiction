# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=1

inherit eutils versionator autotools gnome2-utils

MY_MAJOR=$(get_version_component_range 2)
MY_LETTER=$(get_version_component_range 3)
MY_MINOR=$(get_version_component_range 4)
MY_PATCH=$(get_version_component_range 5)

MY_LOWERS=abcdefghijklmnopqrstuvwxyz
MY_UPPERS=ABCDEFGHIJKLMNOPQRSTUVWXYZ
MY_INDEX=${MY_LOWERS%${MY_LETTER}*}
MY_UPPER=${MY_UPPERS:${#MY_INDEX}:1}

MY_PV=${MY_MAJOR}${MY_UPPER}${MY_MINOR#p}${MY_PATCH:+.${MY_PATCH#p}}
DESCRIPTION="GNOME user interface for Inform 7"
HOMEPAGE="http://inform7.com/ http://sourceforge.net/projects/gnome-inform7/"
SRC_URI="mirror://sourceforge/${PN}/I7_${MY_PV}_GNOME_Source.tar.gz"

LICENSE="GPL-3 LGPL-3 GPL-2 LGPL-2.1 BSD MIT Zarf"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="linguas_es" # optional nls seems broken

RESTRICT="test" # intltool silliness

DEPEND="app-text/gtkspell:2
	dev-libs/dbus-glib
	>=dev-libs/glib-2.16:2
	dev-libs/libxml2
	>=gnome-base/gconf-2.4:2
	net-libs/webkit-gtk:2
	|| ( >=sys-apps/util-linux-2.16
		<sys-libs/e2fsprogs-libs-1.41.8
		<sys-fs/e2fsprogs-1.41.0 )
	virtual/libintl
	>=x11-libs/gdk-pixbuf-2.6:2
	x11-libs/goocanvas:0
	>=x11-libs/gtk+-2.12:2
	>=x11-libs/gtksourceview-2.2:2.0
	x11-libs/pango"
RDEPEND="${DEPEND}
	~dev-lang/inform-${PV%${MY_PATCH:+_${MY_PATCH}}}"
DEPEND="${DEPEND}
	app-arch/xz-utils
	>=dev-util/intltool-0.35.0
	virtual/pkgconfig
	>=sys-devel/gettext-0.17"

S=${WORKDIR}/${PN}-${MY_PV}

src_unpack() {
	unpack ${A}
	cd "${S}"

	epatch "${FILESDIR}"/gnome-inform7-6G60-unbundling.patch

	eautoreconf
}

src_install() {
	emake DESTDIR="${D}" GCONFTOOL=true install || die "emake install failed"

	dodoc AUTHORS ChangeLog NEWS README THANKS TODO || die "dodoc failed"
	docinto frotz
	dodoc src/interpreters/frotz/{AUTHORS,README,TODO} || die "dodoc frotz failed"
	docinto git
	dodoc src/interpreters/git/README.txt || die "dodoc git failed"
	docinto glulxe
	dodoc src/interpreters/glulxe/README || die "dodoc glulxe failed"

	cd "${D}"/usr/share
	rm -r doc/gnome-inform7 || die "rm doc failed"
	rm -r gnome-inform7/Extensions || die "rm Extensions failed"
	rm gnome-inform7/uninstall_manifest.txt || die "rm uninstall_manifest.txt failed"
	mv gnome-inform7/Documentation doc/${PF}/html || die "mv Documentation failed"
	dosym /usr/share/doc/${PF}/html /usr/share/gnome-inform7/Documentation || die "dosym Documentation failed"

	dodir /usr/libexec/gnome-inform7 || die "dodir failed"
	dosym /usr/libexec{,/gnome-inform7}/cBlorb || die "dosym cBlorb failed"
	dosym /usr/bin/inform /usr/libexec/gnome-inform7/inform-6.32-biplatform || die "dosym inform-6.32-biplatform failed"
	dosym /usr/libexec{,/gnome-inform7}/ni || die "dosym ni failed"

	dosym /usr/share/{inform7/Inform7,gnome-inform7}/Extensions || die "dosym Inform7 failed"
}

pkg_preinst() {
	gnome2_gconf_savelist
}

pkg_postinst() {
	gnome2_gconf_install
}

#pkg_prerm() {
#	gnome2_gconf_uninstall
#}
