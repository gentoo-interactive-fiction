# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=1

inherit eutils versionator autotools

MY_MAJOR=$(get_version_component_range 2)
MY_LETTER=$(get_version_component_range 3)
MY_MINOR=$(get_version_component_range 4)

MY_LOWERS=abcdefghijklmnopqrstuvwxyz
MY_UPPERS=ABCDEFGHIJKLMNOPQRSTUVWXYZ
MY_INDEX=${MY_LOWERS%${MY_LETTER}*}
MY_UPPER=${MY_UPPERS:${#MY_INDEX}:1}

MY_PV=${MY_MAJOR}${MY_UPPER}${MY_MINOR#p}
DESCRIPTION="GNOME user interface for Inform 7"
HOMEPAGE="http://inform7.com/ http://sourceforge.net/projects/gnome-inform7/"
SRC_URI="mirror://sourceforge/${PN}/${PN}-${MY_PV}.tar.gz"

LICENSE="GPL-2 LICENSE-BITSTREAM font-bh-ttf"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="=app-text/gtkspell-2*
	dev-libs/dbus-glib
	>=gnome-base/libgnomeui-2
	gnome-extra/gtkhtml:3.14
	media-libs/freetype:2
	media-libs/libpng
	media-libs/libsdl
	media-libs/sdl-mixer
	|| ( >=sys-apps/util-linux-2.16
		<sys-libs/e2fsprogs-libs-1.41.8
		<sys-fs/e2fsprogs-1.41.0 )
	virtual/jpeg:0
	virtual/libintl
	>=x11-libs/gtk+-2.8:2
	x11-libs/gtksourceview:2.0"
RDEPEND="${DEPEND}
	~dev-lang/inform-${PV}
	games-engines/glkfrotz
	games-engines/glulxe"
DEPEND="${DEPEND}
	dev-games/glk-headers
	virtual/pkgconfig
	sys-devel/gettext"

S=${WORKDIR}/${PN}-${MY_PV}

src_unpack() {
	unpack ${A}
	cd "${S}"

	epatch "${FILESDIR}"/gnome-inform7-5J39-CFLAGS.patch
	epatch "${FILESDIR}"/gnome-inform7-5T18-unbundling.patch
	epatch "${FILESDIR}"/gnome-inform7-5T18-welcome.patch
	epatch "${FILESDIR}"/gnome-inform7-5J39-no-ossp-uuid.patch
	epatch "${FILESDIR}"/gnome-inform7-5T18-skein.patch

	eautoreconf

	rm src/gtkterp/garglk/{glk,gi_blorb,gi_dispa,glkstart}.h || die "rm glk headers failed"
}

src_compile() {
	# Don't build libgtkerp-garglk.a: the library is useless except
	# when running under the IDE, and the IDE uses the glkfrotz and
	# glulxe binaries linked against glkloader.
	econf --disable-static || die "econf failed"
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	dodoc AUTHORS ChangeLog NEWS README THANKS TODO || die "dodoc failed"
	dodoc src/gtkterp/{CharterBT.txt,TODO-garglk} || die "dodoc gtkterp failed"

	cd "${D}"/usr/share
	mv gnome-inform7/Documentation doc/${PF}/html || die "mv Documentation failed"
	dosym /usr/share/doc/${PF}/html /usr/share/gnome-inform7/Documentation || die "dosym Documentation failed"

	dosym /usr/{libexec,share/gnome-inform7/Compilers}/cBlorb || die "dosym cBlorb failed"
	dosym /usr/{libexec,share/gnome-inform7/Compilers}/inform-6.31-biplatform || die "dosym inform-6.31-biplatform failed"
	dosym /usr/{libexec,share/gnome-inform7/Compilers}/ni || die "dosym ni failed"

	dosym /usr/share/{,gnome-}inform7/Inform7 || die "dosym Inform7 failed"
	dosym /usr/share/{,gnome-}inform7/Library || die "dosym Library failed"
}
