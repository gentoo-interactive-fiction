# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit eutils versionator autotools gnome2-utils

MY_MAJOR=$(get_version_component_range 2)
MY_LETTER=$(get_version_component_range 3)
MY_MINOR=$(get_version_component_range 4)
MY_PATCH=$(get_version_component_range 5)

MY_LOWERS=abcdefghijklmnopqrstuvwxyz
MY_UPPERS=ABCDEFGHIJKLMNOPQRSTUVWXYZ
MY_INDEX=${MY_LOWERS%${MY_LETTER}*}
MY_UPPER=${MY_UPPERS:${#MY_INDEX}:1}

MY_PV=${MY_MAJOR}${MY_UPPER}${MY_MINOR#p}${MY_PATCH:+.${MY_PATCH#p}}
DESCRIPTION="GNOME user interface for Inform 7"
HOMEPAGE="http://inform7.com/ http://sourceforge.net/projects/gnome-inform7/"
SRC_URI="mirror://sourceforge/${PN}/Gnome_UI_source_${MY_PV}.tar.gz"

LICENSE="GPL-3 LGPL-3 GPL-2 LGPL-2.1 BSD MIT Zarf"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="gstreamer linguas_es linguas_fr" # optional nls seems broken

RESTRICT="test" # intltool silliness

# gstreamer plugins used:
# media-libs/gstreamer: coreelements/typefind
# media-libs/gst-plugins-bad: aiff/aiffparse
# media-libs/gst-plugins-base: audioconvert/audioconvert gio/giostreamsrc ogg/oggdemux volume/volume vorbis/vorbisdec
# media-libs/gst-plugins-good: autodetect/autoaudiosink
# media-plugins/gst-plugins-modplug: modplug/modplug
DEPEND="app-text/gtkspell:2
	dev-libs/dbus-glib
	>=dev-libs/glib-2.32:2
	dev-libs/libxml2
	net-libs/webkit-gtk:2
	|| ( >=sys-apps/util-linux-2.16
		<sys-libs/e2fsprogs-libs-1.41.8
		<sys-fs/e2fsprogs-1.41.0 )
	virtual/libintl
	>=x11-libs/gdk-pixbuf-2.6:2
	x11-libs/goocanvas:0
	>=x11-libs/gtk+-2.24:2
	>=x11-libs/gtksourceview-2.2:2.0
	x11-libs/pango
	gstreamer? (
		media-libs/gstreamer:1.0
		media-libs/gst-plugins-bad:1.0
		media-libs/gst-plugins-base:1.0[ogg,vorbis]
		media-libs/gst-plugins-good:1.0
		media-plugins/gst-plugins-modplug:1.0
	)"
RDEPEND="${DEPEND}
	~dev-lang/inform-${PV%${MY_PATCH:+_${MY_PATCH}}}"
DEPEND="${DEPEND}
	app-arch/xz-utils
	>=dev-util/intltool-0.35.0
	virtual/pkgconfig
	>=sys-devel/gettext-0.17"

S=${WORKDIR}/${PN}-${MY_PV}

src_prepare() {
	epatch "${FILESDIR}"/gnome-inform7-6L02-unbundling.patch

	eautoreconf
}

src_configure() {
	econf --with-sound=$(usex gstreamer gstreamer-1.0 no)
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"

	dodoc AUTHORS ChangeLog NEWS README THANKS TODO || die "dodoc failed"
	docinto frotz
	dodoc src/chimara/interpreters/frotz/{AUTHORS,README,TODO} || die "dodoc frotz failed"
	docinto git
	dodoc src/chimara/interpreters/git/README.txt || die "dodoc git failed"
	docinto glulxe
	dodoc src/chimara/interpreters/glulxe/README || die "dodoc glulxe failed"

	cd "${D}"/usr/share
	rm -r doc/gnome-inform7 || die "rm doc failed"
	rm -r gnome-inform7/Extensions || die "rm Extensions failed"
	rm gnome-inform7/uninstall_manifest.txt || die "rm uninstall_manifest.txt failed"
	mv gnome-inform7/Documentation doc/${PF}/html || die "mv Documentation failed"
	dosym /usr/share/doc/${PF}/html /usr/share/gnome-inform7/Documentation || die "dosym Documentation failed"

	dodir /usr/libexec/gnome-inform7 || die "dodir failed"
	dosym /usr/libexec{,/gnome-inform7}/cBlorb || die "dosym cBlorb failed"
	dosym /usr/bin/inform /usr/libexec/gnome-inform7/inform6 || die "dosym inform6 failed"
	dosym /usr/libexec{,/gnome-inform7}/ni || die "dosym ni failed"

	dosym /usr/share/{inform7/Inform7,gnome-inform7}/Extensions || die "dosym Inform7 failed"
}

pkg_preinst() {
	gnome2_schemas_savelist
}

pkg_postinst() {
	gnome2_schemas_update
}

pkg_postrm() {
	gnome2_schemas_update
}
