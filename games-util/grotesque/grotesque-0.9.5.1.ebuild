# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="6"
PYTHON_COMPAT=( python2_7 )
DISTUTILS_SINGLE_IMPL=yes

inherit eutils distutils-r1

DESCRIPTION="Interactive Fiction library manager"
HOMEPAGE="http://grotesque.invergo.net/"
SRC_URI="http://grotesque.invergo.net/download/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="dev-python/pygobject:3[${PYTHON_USEDEP}]
	dev-python/pyifbabel[${PYTHON_USEDEP}]"
RDEPEND="${DEPEND}
	x11-libs/gtk+:3"

python_prepare() {
	python_fix_shebang .
	sed -i -e "s,doc_dir = .*,doc_dir = '/usr/share/doc/${PF}'," setup.py || die "sed failed"
}

python_install_all() {
	distutils-r1_python_install_all
	rm "${D}/usr/share/doc/${PF}/COPYING" || die "rm COPYING failed"
	doicon src/grotesque/data/grotesque_icon.png
	make_desktop_entry grotesque Grotesque grotesque_icon Game "GenericName=IF Library Manager"
}
